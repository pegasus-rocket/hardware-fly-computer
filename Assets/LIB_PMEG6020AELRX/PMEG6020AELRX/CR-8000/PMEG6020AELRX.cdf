(part "PMEG6020AELRX"
    (packageRef "SODFL3517X110N")
    (interface
        (port "1" (symbPinId 1) (portName "K") (portType INOUT))
        (port "2" (symbPinId 2) (portName "A") (portType INOUT))
    )
    (partClass UNDEF)
    (useInSchema Y)
    (useInLayout Y)
    (inPartsList Y)
    (partType NORMAL)
    (placeRestriction FREE)
    (property "compKind" "221")
    (property "Manufacturer_Name" "Nexperia")
    (property "Manufacturer_Part_Number" "PMEG6020AELRX")
    (property "Mouser_Part_Number" "771-PMEG6020AELRX")
    (property "Mouser_Price/Stock" "https://www.mouser.co.uk/ProductDetail/Nexperia/PMEG6020AELRX?qs=bel5lzaZNxSSRPwNuBp8ow%3D%3D")
    (property "Arrow_Part_Number" "PMEG6020AELRX")
    (property "Arrow_Price/Stock" "https://www.arrow.com/en/products/pmeg6020aelrx/nexperia?region=nac")
    (property "Description" "Nexperia 60V 2.83A, Diode, 2-Pin SOD-123W PMEG6020AELRX")
    (property "Datasheet_Link" "https://assets.nexperia.com/documents/data-sheet/PMEG6020AELR.pdf")
    (property "symbolName1" "PMEG6020AELRX")
)
