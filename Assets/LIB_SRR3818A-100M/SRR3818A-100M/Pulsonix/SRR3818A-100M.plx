PULSONIX_LIBRARY_ASCII "SamacSys ECAD Model"
//885457/1304982/2.50/2/4/Inductor

(asciiHeader
	(fileUnits MM)
)
(library Library_1
	(padStyleDef "r430_160"
		(holeDiam 0)
		(padShape (layerNumRef 1) (padShapeType Rect)  (shapeWidth 1.600) (shapeHeight 4.300))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 0) (shapeHeight 0))
	)
	(textStyleDef "Normal"
		(font
			(fontType Stroke)
			(fontFace "Helvetica")
			(fontHeight 1.27)
			(strokeWidth 0.127)
		)
	)
	(patternDef "SRR3818A1R0Y" (originalName "SRR3818A1R0Y")
		(multiLayer
			(pad (padNum 1) (padStyleRef r430_160) (pt 0.000, 1.350) (rotation 90))
			(pad (padNum 2) (padStyleRef r430_160) (pt 0.000, -1.350) (rotation 90))
		)
		(layerContents (layerNumRef 18)
			(attr "RefDes" "RefDes" (pt 0.000, 0.000) (textStyleRef "Normal") (isVisible True))
		)
		(layerContents (layerNumRef 28)
			(line (pt -1.9 1.9) (pt 1.9 1.9) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt 1.9 1.9) (pt 1.9 -1.9) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt 1.9 -1.9) (pt -1.9 -1.9) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt -1.9 -1.9) (pt -1.9 1.9) (width 0.025))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -3.15 3.15) (pt 3.15 3.15) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 3.15 3.15) (pt 3.15 -3.15) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 3.15 -3.15) (pt -3.15 -3.15) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -3.15 -3.15) (pt -3.15 3.15) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt 1.9 0.2) (pt 1.9 -0.1) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -1.9 0.2) (pt -1.9 -0.1) (width 0.1))
		)
	)
	(symbolDef "SRR3818A-100M" (originalName "SRR3818A-100M")

		(pin (pinNum 1) (pt 0 mils 0 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName false)) (pinName (text (pt 0 mils -35 mils) (rotation 0]) (justify "UpperLeft") (textStyleRef "Normal"))
		))
		(pin (pinNum 2) (pt 800 mils 0 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName false)) (pinName (text (pt 800 mils -35 mils) (rotation 0]) (justify "UpperRight") (textStyleRef "Normal"))
		))
		(arc (pt 250 mils -2 mils) (radius 50 mils) (startAngle 177.7) (sweepAngle -175.4) (width 6 mils))
		(arc (pt 350 mils -2 mils) (radius 50 mils) (startAngle 177.7) (sweepAngle -175.4) (width 6 mils))
		(arc (pt 450 mils -2 mils) (radius 50 mils) (startAngle 177.7) (sweepAngle -175.4) (width 6 mils))
		(arc (pt 550 mils -2 mils) (radius 50 mils) (startAngle 177.7) (sweepAngle -175.4) (width 6 mils))
		(attr "RefDes" "RefDes" (pt 650 mils 250 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))
		(attr "Type" "Type" (pt 650 mils 150 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))

	)
	(compDef "SRR3818A-100M" (originalName "SRR3818A-100M") (compHeader (numPins 2) (numParts 1) (refDesPrefix L)
		)
		(compPin "1" (pinName "1") (partNum 1) (symPinNum 1) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "2" (pinName "2") (partNum 1) (symPinNum 2) (gateEq 0) (pinEq 0) (pinType Unknown))
		(attachedSymbol (partNum 1) (altType Normal) (symbolName "SRR3818A-100M"))
		(attachedPattern (patternNum 1) (patternName "SRR3818A1R0Y")
			(numPads 2)
			(padPinMap
				(padNum 1) (compPinRef "1")
				(padNum 2) (compPinRef "2")
			)
		)
		(attr "Manufacturer_Name" "Bourns")
		(attr "Manufacturer_Part_Number" "SRR3818A-100M")
		(attr "Mouser Part Number" "652-SRR3818A-100M")
		(attr "Mouser Price/Stock" "https://www.mouser.com/Search/Refine.aspx?Keyword=652-SRR3818A-100M")
		(attr "Arrow Part Number" "SRR3818A-100M")
		(attr "Arrow Price/Stock" "https://www.arrow.com/en/products/srr3818a-100m/bourns")
		(attr "Description" "Fixed Inductors 1.3A 10uH 20% SMD 3818 AEC-Q200")
		(attr "<Hyperlink>" "https://componentsearchengine.com/Datasheets/2/SRR3818A-1R0Y.pdf")
		(attr "<Component Height>" "2")
	)

)
